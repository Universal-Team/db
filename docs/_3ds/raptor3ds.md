---
author: RetroGamer02/Ryan
avatar: https://avatars.githubusercontent.com/u/70994866?v=4
categories:
- game
color: '#625456'
color_bg: '#625456'
created: '2022-09-10T22:26:05Z'
description: Raptor Call Of The Shadows Console Ports from Reversed-engineered source
  code
download_filter: 3DS
download_page: https://github.com/RetroGamer02/raptor-consoles/releases
downloads:
  Raptor3DS.V1.0.3.zip:
    size: 4142955
    size_str: 3 MiB
    url: https://github.com/RetroGamer02/raptor-consoles/releases/download/1.0.3/Raptor3DS.V1.0.3.zip
github: RetroGamer02/raptor-consoles
icon: https://raw.githubusercontent.com/RetroGamer02/raptor3ds/master/rapicon.png
image: https://raw.githubusercontent.com/RetroGamer02/raptor3ds/master/RapBanner.png
image_length: 37938
layout: app
license: gpl-2.0
license_name: GNU General Public License v2.0
screenshots:
- description: Main menu
  url: https://db.universal-team.net/assets/images/screenshots/raptor3ds/main-menu.png
script_message: 'Note: You will need "FILE0002.GLB", "FILE0003.GLB",

  and "FILE0004.GLB" from the v1.2 DOS version the in

  the sdmc:/3ds/Raptor folder to play the game.'
source: https://github.com/RetroGamer02/raptor-consoles
systems:
- 3DS
title: Raptor3DS
update_notes: '<p dir="auto">Rebased Raptor3DS to the Current Source code by Skynettx.<br>

  In the process I redid some things better than I did the first time and fixed a
  few issues.</p>

  <p dir="auto">Can now skip all parts of the intro.<br>

  Entering shop responds to button press properly.<br>

  Fixed a crash when using right trigger on callsign entry field.</p>

  <p dir="auto">In general code should run better and be more readable.<br>

  Special thanks to Skynettx for their continued work on the Raptor Source code!</p>'
updated: '2023-03-09T02:35:20Z'
version: 1.0.3
version_title: Rebase
---
Reversed-engineered source port from Raptor Call Of The Shadows ported to the Nintendo 3DS.