---
author: DS-Homebrew
avatar: https://avatars.githubusercontent.com/u/46971470?v=4
categories:
- utility
color: '#464061'
color_bg: '#464061'
created: '2017-05-06T05:28:36Z'
description: DSi Menu replacement for DS/DSi/3DS/2DS
download_page: https://github.com/DS-Homebrew/TWiLightMenu/releases
downloads:
  TWiLightMenu-3DS.7z:
    size: 33193255
    size_str: 31 MiB
    url: https://github.com/DS-Homebrew/TWiLightMenu/releases/download/v25.9.4/TWiLightMenu-3DS.7z
  TWiLightMenu-DSi.7z:
    size: 33242358
    size_str: 31 MiB
    url: https://github.com/DS-Homebrew/TWiLightMenu/releases/download/v25.9.4/TWiLightMenu-DSi.7z
  TWiLightMenu-Flashcard.7z:
    size: 44119422
    size_str: 42 MiB
    url: https://github.com/DS-Homebrew/TWiLightMenu/releases/download/v25.9.4/TWiLightMenu-Flashcard.7z
  TWiLightMenu.7z:
    size: 44325573
    size_str: 42 MiB
    url: https://github.com/DS-Homebrew/TWiLightMenu/releases/download/v25.9.4/TWiLightMenu.7z
github: DS-Homebrew/TWiLightMenu
icon: https://raw.githubusercontent.com/DS-Homebrew/TWiLightMenu/master/booter/Twilight%2B%2B-animated%20icon-fix.gif
icon_static: https://db.universal-team.net/assets/images/icons/twilight-menu.png
image: https://db.universal-team.net/assets/images/images/twilight-menu.png
image_length: 12520
layout: app
license: gpl-3.0
license_name: GNU General Public License v3.0
nightly:
  download_page: https://github.com/TWLBot/Builds
  downloads:
    TWiLightMenu-3DS.7z:
      url: https://github.com/TWLBot/Builds/raw/master/TWiLightMenu-3DS.7z
    TWiLightMenu-DSi.7z:
      url: https://github.com/TWLBot/Builds/raw/master/TWiLightMenu-DSi.7z
    TWiLightMenu-Flashcard.7z:
      url: https://github.com/TWLBot/Builds/raw/master/TWiLightMenu-Flashcard.7z
    TWiLightMenu.7z:
      url: https://github.com/TWLBot/Builds/raw/master/TWiLightMenu.7z
priority: true
source: https://github.com/DS-Homebrew/TWiLightMenu
systems:
- DS
title: TWiLight Menu++
update_notes: '<p dir="auto">Check here on how to update <strong>TW</strong>i<strong>L</strong>ight
  Menu++:</p>

  <ul dir="auto">

  <li><a href="https://wiki.ds-homebrew.com/twilightmenu/updating-flashcard.html"
  rel="nofollow">Flashcard</a></li>

  <li><a href="https://wiki.ds-homebrew.com/twilightmenu/updating-dsi.html" rel="nofollow">DSi</a></li>

  <li><a href="https://wiki.ds-homebrew.com/twilightmenu/updating-3ds.html" rel="nofollow">3DS</a></li>

  </ul>

  <h3 dir="auto">Bug fix</h3>

  <ul dir="auto">

  <li><strong>Now applies to DS Classic Menu as well:</strong> Fixed an overlooked
  bug which made games not start.

  <ul dir="auto">

  <li>For DS(i) games, nds-bootstrap would either display error code -2 or white screens
  after launching a game.</li>

  </ul>

  </li>

  </ul>'
updated: '2023-04-21T05:49:41Z'
version: v25.9.4
version_title: 'v25.9.4 (hotfix #2)'
website: https://wiki.ds-homebrew.com/twilightmenu/
wiki: https://wiki.ds-homebrew.com/twilightmenu/
---
TWiLight Menu++ is an open-source DSi Menu upgrade/replacement for the Nintendo DSi, the Nintendo 3DS, and Nintendo DS flashcards. It can launch Nintendo DS, SNES, NES, GameBoy (color), GameBoy Advance, Sega GameGear/Master System & Mega Drive/Genesis ROMs, as well as DSTWO plugins (if you use a DSTWO) and videos.

Please check the [wiki](https://wiki.ds-homebrew.com/twilightmenu/) for help installing.